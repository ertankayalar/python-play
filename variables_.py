'''
This is a multine comment
or 
docstring (used to define a function purpose)
can be single or double quotes
'''

"""
Variable Rules:
    - Variable names are case sensitive (name and NAME are different variables)
    - Must start with a letter or an underscore
    - Can have numbers but can not start with one
"""

x = 10              # int
y = 10.5            # float 
name = 'Ertan'      # string
is_online = True    # bool

# Multiple assignment

price, year, firstname, lastname, is_published = (50.3, 2023, 'Ertan', 'Kayalar', False)

print(price, year, firstname, lastname, is_published) 

# Basic  math

total = x + y

print(total)

# Casting
x = str(year)
y = int(price)
z = float(price)

# check type
print(type(z))


